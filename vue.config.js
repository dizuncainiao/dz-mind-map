module.exports = {
  "css": {
    "extract": false
  },
  "transpileDependencies": [
    "vuetify"
  ],
  publicPath: process.env.NODE_ENV === 'development' ? '' : '/dz-mind-map',
}
